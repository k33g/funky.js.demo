const testIt = require('./test-it')

let f = testIt.getFunctionFrom("./hello-function.js")

testIt.assert("sam cmd, first item is salad", f({name:"sam"})[0] == "salade")
testIt.assert("sam cmd, first item is tomates", f({name:"sam"})[1] == "tomates")
testIt.assert("sam cmd, first item is veau", f({name:"sam"})[2] == "veau")

testIt.end()