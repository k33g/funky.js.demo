(params) => {
  let name = params.name;
  let cmds = {
    bob: ['salade', 'tomates', 'oignons', 'veau', 'harissa'],
    sam: ['salade', 'tomates', 'veau'],
    john: ['salade', 'veau']
  }
  return cmds[name]
}
